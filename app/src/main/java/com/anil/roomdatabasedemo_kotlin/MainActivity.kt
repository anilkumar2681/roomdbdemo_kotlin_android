package com.anil.roomdatabasedemo_kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.anil.roomdatabasedemo_kotlin.dao.GenderDao
import com.anil.roomdatabasedemo_kotlin.database.AppDataBase
import com.anil.roomdatabasedemo_kotlin.entity.Gender
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var db:AppDataBase?=null
    private var genderDao:GenderDao?=null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        db= AppDataBase.getAppDataBase(context = this)

        Observable.fromCallable({
            db= AppDataBase.getAppDataBase(context = this)
            genderDao = db?.genderDao()

            val gender1 = Gender(name = "Male")
            val gender2 = Gender(name = "Female")

            with(genderDao){
                this?.insertGender(gender1)
                this?.insertGender(gender2)
            }
            db?.genderDao()?.getGenders()

        }).doOnNext({list->
            var finalString = ""
            list?.map { finalString+= it.name+" - " }
            tv_message.text = finalString
        }).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }
}
