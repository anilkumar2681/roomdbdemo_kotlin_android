package com.anil.roomdatabasedemo_kotlin.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.anil.roomdatabasedemo_kotlin.converter.DateTypeConverter
import com.anil.roomdatabasedemo_kotlin.converter.ListConverter
import com.anil.roomdatabasedemo_kotlin.dao.ExerciseDao
import com.anil.roomdatabasedemo_kotlin.dao.GenderDao
import com.anil.roomdatabasedemo_kotlin.dao.RoutineDao
import com.anil.roomdatabasedemo_kotlin.dao.TraineeDao
import com.anil.roomdatabasedemo_kotlin.entity.Exercise
import com.anil.roomdatabasedemo_kotlin.entity.Gender
import com.anil.roomdatabasedemo_kotlin.entity.Routine
import com.anil.roomdatabasedemo_kotlin.entity.Trainee


@Database (entities = [Exercise::class, Gender::class,Routine::class,Trainee::class],version = 1)
@TypeConverters(DateTypeConverter::class,ListConverter::class)

abstract class AppDataBase:RoomDatabase() {

    abstract fun excerciseDao():ExerciseDao
    abstract fun genderDao(): GenderDao
    abstract fun routineDao():RoutineDao
    abstract fun traineeDao():TraineeDao

    companion object{

        var INSTANCE:AppDataBase?=null

        fun getAppDataBase(context: Context):AppDataBase?{
            if (INSTANCE==null){
                synchronized(AppDataBase::class){
                    INSTANCE=Room.databaseBuilder(context.applicationContext,AppDataBase::class.java,"myDB").build()
                }
            }
            return INSTANCE
        }

        fun destroyDataBase(){
            INSTANCE=null
        }

    }
}