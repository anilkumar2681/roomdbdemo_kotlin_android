package com.anil.roomdatabasedemo_kotlin.dao

import androidx.room.*
import com.anil.roomdatabasedemo_kotlin.entity.Trainee


@Dao
interface TraineeDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertTrainee(trainee: Trainee)

    @Update
    fun updateTrainee(trainee: Trainee)

    @Delete
    fun deleteTrainee(trainee: Trainee)

    @Query("SELECT * FROM Trainee WHERE name = :nameToFind")
    fun getUserByName(nameToFind: String): List<Trainee>
}